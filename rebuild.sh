#!/bin/bash

set -e

lsblk

echo "Is /dev/sdb1 ok? If not, change this script!"
read -n1 -r -p "Press space to continue..." key

if [ "$key" = '' ]; then
    make
    make modules
    mount /dev/sdb1 /mnt/TK1SOM
    ./update_kernel.sh
    make modules_install INSTALL_MOD_PATH=/mnt/TK1SOM
    umount /dev/sdb1
else
    echo "Cancelled"
fi
